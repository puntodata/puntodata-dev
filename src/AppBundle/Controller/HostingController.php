<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
class HostingController extends Controller
{
    
     /**
     * @Route("/hosting", name="host")
     */
    public function indexAction(Request $request)
    {
        return $this->render('hosting/index.html.twig');
    }
}
