var $slider = $('.slider');

var $slickTrack = $('.slick-track');
var $slickCurrent = $('.slick-current');

var slideDuration = 900;

//RESET ANIMATIONS
// On init slide change
$slider.on('init', function(slick) {
    TweenMax.to($('.slick-track'), 0.9, {
        marginLeft: 0
    });
    TweenMax.to($('.slick-active'), 0.9, {
        x: 0,
        zIndex: 2
    });
});
// On before slide change
$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    TweenMax.to($('.slick-track'), 0.9, {
        marginLeft: 0
    });
    TweenMax.to($('.slick-active'), 0.9, {
        x: 0
    });
});

// On after slide change
$slider.on('afterChange', function(event, slick, currentSlide) {
    TweenMax.to($('.slick-track'), 0.9, {
        marginLeft: 0
    });
    $('.slick-slide').css('z-index', '1');
    TweenMax.to($('.slick-active'), 0.9, {
        x: 0,
        zIndex: 2
    });
});
var time = 400;
$('.slick-prev').click(function() {
    $('.slick-current').removeClass('slick-active')

    setTimeout(function() {
        $('.slider').slick('slickPrev');
        if (!$('.slick-active').hasClass('dinamy-cel')) {
            TweenMax.to($('.slider-sub-contentEl-1'), 0.6, {
                x: $(window).width(),
                ease: Quad.easeOut
            });
        } else if ($('.slick-active').hasClass('dinamy-cel')) {
            TweenMax.to($('.slider-sub-contentEl-1'), 0.6, {
                x: 0,
                ease: Quad.easeOut
            });
        }
    }, time)

})
$('.slick-next').click(function() {

        $('.slick-current').removeClass('slick-active')
        setTimeout(function() {
            var w = $(window).width()
            $('.slider').slick('slickNext');
            if (!$('.slick-active').hasClass('dinamy-cel')) {
                TweenMax.to($('.slider-sub-contentEl-1'), 0.6, {
                    x: -w,
                    ease: Quad.easeOut
                });
            } else if ($('.slick-active').hasClass('dinamy-cel')) {
                TweenMax.to($('.slider-sub-contentEl-1'), 0.6, {
                    x: 0,
                    ease: Quad.easeOut
                });
            }
        }, time)

    })
    //SLICK INIT
$('.slider').slick({
    speed: slideDuration,
    dots: true,
    waitForAnimate: true,
    useTransform: true,
    prevArrow: false,
    nextArrow: false,
    //autoplay: true,
    cssEase: 'cubic-bezier(0.455, 0.030, 0.130, 1.000)'
})
var box = 5;

function directionRight(to, val) {
    $('.rounder-ball-1').css('left', '0')
}

var wh = $(window).width();

function boxing(d1, d2) {
    $('#box0').css({
        'transform': 'translateX(' + d1 + 'px ) translateY(-50%)'
    })
    $('#box1').css({
        'transform': 'translateX(' + d2 + 'px ) translateY(-50%)'
    })
}
$(".slider").on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    var dataId = $('.slick-current').attr("data-slick-index");
    if (Math.abs(nextSlide - currentSlide) == 1) {
        direction = (nextSlide - currentSlide > 0) ? "right" : "left";
    } else {
        direction = (nextSlide - currentSlide > 0) ? "left" : "right";
    }

    if (direction == 'right') {

        if (dataId == 0) {


        } else if (dataId == 1) {
            boxing(40, 30);
        }


    } else if (direction == 'left') {



    }





})



$('.slick-prev').mouseover(function() {
    var dataId = $('.slick-current').attr("data-slick-index");
    if (dataId == 0) {
        boxing(40, 30);
    }

})
$('.slick-prev').mouseleave(function() {
    var dataId = $('.slick-current').attr("data-slick-index");
    if (dataId == 0) {
        boxing(10, 10);
    }
})


$('.slick-next').mouseover(function() {
    $('.slick-prev').addClass('hidden-none')
    var dataId = $('.slick-current').attr("data-slick-index");
    if (dataId == 0) {
        boxing(-40, -30);
    }
    if (dataId == 1) {
        boxing(-60, -50);
    }

})
$('.slick-next').mouseleave(function() {
    $('.slick-prev').removeClass('hidden-none')

    var dataId = $('.slick-current').attr("data-slick-index");
    if (dataId == 0) {
        boxing(-10, -10);
    }
    if (dataId == 1) {
        boxing(-30, -30);
    }
})

if ($(window).width() < 768) {

} else {
    //PREV
    $('.slick-prev').on('mouseenter', function() {
        TweenMax.to($('.slick-track'), 0.6, {
            marginLeft: "180px",
            ease: Quad.easeOut
        });
        TweenMax.to($('.slick-current'), 0.6, {
            x: -100,
            ease: Quad.easeOut
        });

        if ($('.slick-active').hasClass('dinamy-cel')) {
            TweenMax.to($('.slider-sub-contentEl-1'), 0.6, {
                x: 100,
                ease: Quad.easeOut
            });
        }
    });



    $('.slick-prev').on('mouseleave', function() {
        if ($('.slick-active').hasClass('dinamy-cel')) {
            TweenMax.to($('.slider-sub-contentEl-1'), 0.6, {
                x: 0,
                ease: Quad.easeOut
            });
        }
        TweenMax.to($('.slick-track'), 0.4, {
            marginLeft: 0,
            ease: Sine.easeInOut
        });
        TweenMax.to($('.slick-current'), 0.4, {
            x: 0,
            ease: Sine.easeInOut
        });
    });

    //NEXT
    $('.slick-next').on('mouseenter', function() {
        if ($('.slick-active').hasClass('dinamy-cel')) {
            TweenMax.to($('.slider-sub-contentEl-1'), 0.6, {
                x: -100,
                ease: Quad.easeOut
            });
        }
        TweenMax.to($('.slick-track'), 0.6, {
            marginLeft: "-180px",
            ease: Quad.easeOut
        });
        TweenMax.to($('.slick-current'), 0.6, {
            x: 100,
            ease: Quad.easeOut
        });
    });

    $('.slick-next').on('mouseleave', function() {
        if ($('.slick-active').hasClass('dinamy-cel')) {
            TweenMax.to($('.slider-sub-contentEl-1'), 0.6, {
                x: 0,
                ease: Quad.easeOut
            });
        }
        TweenMax.to($('.slick-track'), 0.4, {
            marginLeft: 0,
            ease: Quad.easeInOut
        });
        TweenMax.to($('.slick-current'), 0.4, {
            x: 0,
            ease: Quad.easeInOut
        });
    });
}


$('.slick-next').on('click', function(e) {
    e.stopPropagation(); // use this
});