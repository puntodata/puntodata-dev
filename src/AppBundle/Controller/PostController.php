<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

use AppBundle\Entity\Contact;

/**
 * Post controller.
 *
 */
class PostController extends Controller
{


    /**
     * Lists all post entities.
     *
     * @Route("/blog", name="blog")
     * @Method("GET")
     */
    public function indexBlogAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Post')->createQueryBuilderWithCategory()
            ->getQuery();

        $posts = $this->get('knp_paginator')->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

       
        return $this->render('post/index.html.twig', array(
            'posts' => $posts,
            'page' => $request->query->getInt('page', 1)));
    }
    /**
     * Lists all post entities.
     *
     * @Route("/", name="root")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Post')->createQueryBuilderWithCategory()
            ->getQuery();

        $posts = $this->get('knp_paginator')->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        //Contact

        $contact = new Contact;
      
      # Add form fields
        $form = $this->createFormBuilder($contact)
        ->add('name', TextType::class, array(
            
            'attr' => array(
                'class' => 'form-control', 
                'placeholder' => 'Nombre',
                )))
        ->add('email', TextType::class, array(
        'attr' => array(
            'class' => 'form-control', 
            'placeholder' => 'Correo',
           )))
        ->add('subject', TextType::class, array('attr' => 
        array(
        'class' => 'form-control', 
        'placeholder' => 'Asunto',
        )))
        ->add('message', TextareaType::class, array('attr' => array(
            'placeholder' => 'Mensaje',
            'class' => 'form-control')))
        ->add('Save', ButtonType::class, array('label'=> 'Enviar', 'attr' => array(
            'class' => 'btn-footer',
         )))
        ->getForm();
        
      # Handle form and recaptcha response
        $form->handleRequest($request);

        return $this->render('home/index.html.twig', array(
            'form' => $form->createView(),
            'posts' => $posts,
            'page' => $request->query->getInt('page', 1)));
    }

    /**
     * Finds and displays a post entity.
     *
     * @Route("/post/{slug}", name="post_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository("AppBundle:Post")->createQueryBuilderWithUserAndCategory()
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleResult();

        $comment = new Comment();
        $comment->setPost($post);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setPost($post);
            $em->persist($comment);
            $em->flush();
            $this->addFlash('success', 'Thanks for your comment');
            return $this->redirectToRoute('post_show', ['slug' => $post->getSlug()]);
        }

        return $this->render('post/show.html.twig', [
            'comment_form' => $form->createView(),
            'post' => $post
        ]);
    }

    /**
     * @Route("/author/{id}", name="post_author")
     * @Method("GET")
     */
    public function authorAction(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Post')->createQueryBuilderWithCategory()
            ->where("p.user = :user")
            ->setParameter("user", $user)
            ->getQuery();

        $posts = $this->get('knp_paginator')->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('post/index.html.twig', [
            'author' => $user,
            'posts' => $posts,
            'page' => $request->query->getInt('page', 1)
        ]);
    }

    /**
     * @Route("/category/{slug}", name="post_category")
     * @Method("GET")
     */
    public function categoryAction(Request $request, Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Post')->createQueryBuilderWithUser()
            ->where("p.category = :category")
            ->setParameter("category", $category)
            ->getQuery();

        $posts = $this->get('knp_paginator')->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );
        

        return $this->render('post/index.html.twig', [
            'category' => $category,
            'posts' => $posts,
            'page' => $request->query->getInt('page', 1)
        ]);
    }

    public function sidebarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AppBundle:Category')->findAll();
        $posts = $em->getRepository('AppBundle:Post')->findBy(
            [],
            ['createdAt' => 'DESC'],
            2,
            0
        );
        return $this->render('partials/sidebar.html.twig', [
            'categories' => $categories,
            'posts' => $posts
        ]);
    }

}
