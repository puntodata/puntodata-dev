<?php

namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Team;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
class TeamController extends Controller
{

    /**
     * Lists all post entities.
     *
     * @Route("/admin/team", methods={"GET", "POST"}, name="team")
     * 
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Team')->createQueryAll()->getQuery()->getResult();

        
        return $this->render('AdminBundle::team/index.html.twig', [
            'teams' => $query
        ]);

    
    }

    /**
     * Lists all post entities.
     *
     * @Route("/admin/team/new", methods={"GET", "POST"}, name="new_team")
     *
     */
    public function newTeamAction(Request $request)
    {
        $team = new Team;
      
      # Add form fields
        $form = $this->createFormBuilder($team)
        ->add('name', TextType::class, array(
            
            'attr' => array(
                'class' => 'form-control', 
                'placeholder' => 'Nombre',
                )))
        ->add('puesto', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control', 
                    'placeholder' => 'Nombre',
                )))
        ->add('description', TextType::class, array(
            'attr' => array(
                'class' => 'form-control', 
                'placeholder' => 'Nombre',
            )))
        ->add('my_file', FileType::class, [
            'mapped' => false,
            'label' => 'name'
        ])
        ->add('instagram', TextType::class, array(
            'attr' => array(
                'class' => 'form-control', 
                'placeholder' => 'Nombre',
        )))
        ->add('facebook', TextType::class, array(
            'attr' => array(
                'class' => 'form-control', 
                'placeholder' => 'Nombre',
        )))
        ->add('linkeding', TextType::class, array(
            'attr' => array(
                'class' => 'form-control', 
                'placeholder' => 'Nombre',
        )))
        ->add('cv', TextType::class, array(
            'attr' => array(
                'class' => 'form-control', 
                'placeholder' => 'Nombre',
        )))
        ->add('Save', SubmitType::class, array('label'=> 'Enviar', 'attr' => array(
            'class' => 'btn-footer',
         )))
        ->getForm();
        
      # Handle form and recaptcha response
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['my_file']->getData();
            $ext = $file->guessExtension();
            $file_name =  time().".".$ext;
            $file->move('image_directory', $file_name);  

 

            $em = $this->getDoctrine()->getManager();
            $team->setImage($file_name);
            $em->persist($team);
            $em->flush();
            $this->addFlash('success', 'Post created successfully');
            return $this->redirectToRoute('new_team');
        }
        
        return $this->render('AdminBundle::team/new.html.twig', [
            'form' => $form->createView()
        ]);

    
    }

}
