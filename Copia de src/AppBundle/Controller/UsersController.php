<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\RegisterType;

class UsersController extends Controller
{

    /**
     * @Route("/register", name="registers")
     **/
    public function registerAction(Request $request){
        $user  = new User();
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted()){
           
            $user->setRoles('ROLE_USER');
            

        }
        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
