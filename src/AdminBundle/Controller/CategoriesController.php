<?php

namespace AdminBundle\Controller;
use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class CategoriesController extends Controller
{
     /**
     * Lists all post entities.
     *
     * @Route("/categories", name="categories")
     * 
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Category')->createQueryBuilderCategory();
        $paginator  = $this->get('knp_paginator');
        $category = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('AdminBundle::categories/lista.html.twig', array(
            'categories' => $category,
           ));
        
    }
}
