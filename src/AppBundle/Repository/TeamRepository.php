<?php

namespace AppBundle\Repository;
use AppBundle\Entity\Team;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
/**
 * TeamRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TeamRepository extends \Doctrine\ORM\EntityRepository
{

     /**
     * Add a fetchmode Eager for categories
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createQueryAll() {
        return $this->createQueryBuilder("u")
            ->orderBy("u.id", "DESC");
    }

}
