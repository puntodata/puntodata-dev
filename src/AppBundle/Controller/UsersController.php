<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
class UsersController extends Controller
{
    /**
     * @Route("admin/user", name="user")
     **/

     
    public function indexAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:User')->createQueryAll()->getQuery()->getResult();

        
        return $this->render('AdminBundle::users/user.html.twig', [
            'user' => $query
        ]);
    }   
}
