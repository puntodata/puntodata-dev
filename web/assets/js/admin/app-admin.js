'use strict';

function onScroll(event) {
    var scrollPos = $(document).scrollTop();
    $('.active-smooth-scroll .item-li a').each(function () {
        var currLink = $(this);

        var refElement = $(currLink.attr("href"));
        console.log(refElement);
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('.active-smooth-scroll .item-li a').removeClass("expand-active");
            currLink.addClass("expand-active");
        } else {
            currLink.removeClass("expand-active");
        }
    });
}

$(document).ready(function () {
    $(document).on("scroll", onScroll);
    //smoothscroll
    $('.active-smooth-scroll .item-li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('.active-smooth-scroll .item-li a').each(function () {
            $(this).removeClass('expand-active');
        });
        $(this).addClass('expand-active');

        var target = this.hash,
            menu = target;
        var $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - 30
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });

    /*=========================================================================
       Collapse cambiar contraseña
    =========================================================================*/

    $('.toggleCollapse').click(function () {
        $('.contrasena-toggle').toggleClass('hide');
    });

    /*=========================================================================
       promociones-collapse
    =========================================================================*/
    $('.promociones-collapse').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().next().toggle("slow", function () {});
    });
    $('#change-contrasena').click(function () {

        $('#hide-contrasena').toggle("slow", function () {
            // Animation complete.
        });
    });

    /*=========================================================================
     Codigo Externo
    =========================================================================*/

    if (document.querySelectorAll('.drop-drop').length) {
        var openDrop = function openDrop() {
            var containerEl = this.parentElement;

            if (hasClass(containerEl, 'open-drop')) {
                containerEl.classList.remove('open-drop');
            } else {
                $('.container-dropdown').each(function () {
                    $(this).removeClass('open-drop');
                });
                containerEl.classList.add('open-drop');
            }
        };

        var drop = document.querySelectorAll('.drop-drop');

        for (var i = 0; i < drop.length; i++) {
            drop[i].addEventListener('click', openDrop, false);
        }
    }
});